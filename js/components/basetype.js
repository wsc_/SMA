//CONTROLS MAP
var controls = 
`<ul class="controls_map">
    <li><a href="#"><i class="fa fa-3x fa-globe" aria-hidden="true"></i></a></li>
    <li><a href="#"><i class="fa fa-3x fa-search-plus" aria-hidden="true"></i></a></li>
    <li><a href="#"><i class="fa fa-3x fa-search-minus" aria-hidden="true"></i></a></li>
    <li><a href="#"><i class="fa fa-3x fa-arrow-left" aria-hidden="true"></i></a></li>
    <li><a href="#"><i class="fa fa-3x fa-arrow-right" aria-hidden="true"></i></a></li>
    <li><a href="#"><i class="fa fa-3x fa-print" aria-hidden="true"></i></a></li>
    <li><a href="#"><i class="fa fa-3x fa-info-circle" aria-hidden="true"></i></a></li>
</ul>`;

//FLOTANS
var flotant_ibt1 = 
`<div class="info_base_tipo1 col-md-4 flotant">
    <div class="flotant_header">
        <p class="ibt_title">Informacion base tipo 1</p>
        <div class="min_close_container">
            <span data-cx="_" class="close">_</span>
            <span data-cx="X" class="close">X</span>
        </div>
    </div>
    <div class="flotant_body">
        <ul class="ibt1_list">
            <li class="ibt1_sinanpe_item"><label for="check_sinanpe">SINANPE</label>
                <input class="ibt_check" type="checkbox" name="check_sinanpe" id="check_sinanpe">
                <ul class="ibt1_sinanpe_list">
                    <li><label for="check_anp">Áreas naturales protegidas definitivas(ANP)</label>
                        <input class="ibt_check" type="checkbox" name="check_anp" id="check_anp">
                        <ul class="anp_list">
                            <li>Parques Nacionales</li>
                            <li>Santuario Nacionales</li>
                            <li>Santuarios Históricos</li>
                            <li>Reservas Nacionales</li>
                            <li>Refugios de Vida Silvestre</li>
                            <li>Reservas Paisajísticas</li>
                            <li>Reservas Comunales</li>
                            <li>Bosques de Protección</li>
                            <li>Cotos de Caza</li>
                        </ul>
                    </li>
                    <li><label for="check_anpt">Áreas Naturales Protegidas transitorias</label>
                        <input class="ibt_check" type="checkbox" name="check_anpt" id="check_anpt">
                        <ul class="anpt_list">
                            <li>Zonas Reservadas(ZR)</li>
                        </ul>
                    </li>
                    <li><label for="check_acr">Áreas de Conservación Regional(ACR)</label>
                        <input class="ibt_check" type="checkbox" name="check_acr" id="check_acr">
                    </li>
                    <li><label for="check_acp">Áreas de Conservación Privada(ACP)</label>
                        <input class="ibt_check" type="checkbox" name="check_acp" id="check_acp">
                    </li>
                </ul>
            </li>
            <li class="ibt1_sinanpe_item"><label for="check_oaa">Otras áreas para el analisis</label>
                <input class="ibt_check" type="checkbox" name="check_oaa" id="check_oaa">
                <ul class="oaa_ist">
                    <li><a href="#">Concesiones de conservación</a>
                        <input class="ibt_check" type="checkbox" name="check_cdc">
                    </li>
                    <li><a href="#">Concesiones de ecoturismo</a>
                        <input class="ibt_check" type="checkbox" name="check_cde" >
                    </li>
                    <li><a href="#">Concesiones de aprovechamiento forestal</a>
                        <input class="ibt_check" type="checkbox" name="">
                    </li>
                    <li><a href="#">Reservas Territoriales</a>
                        <input class="ibt_check" type="checkbox" name="check_rt">
                    </li>
                    <li><a href="#">Reservas indígenas</a>
                        <input class="ibt_check" type="checkbox" name="check_ris">
                    </li>
                    <li><a href="#">Zonas de amortiguamiento(ZA)</a>
                        <input class="ibt_check" type="checkbox" name="check_za">
                    </li>
                    <li><a href="#">Reservas de Biosfera</a>
                        <input class="ibt_check" type="checkbox" name="check_rb">
                    </li>
                    <li><a href="#">Sitios de Patrimonio Mundial Natural de la UNESCO</a>
                        <input class="ibt_check" type="checkbox" name="check_spmnu" >
                    </li>
                    <li><a href="#">Sitios de Patrimonio(Cultural y Natural) de la UNESCO</a>
                        <input class="ibt_check" type="checkbox" name="check_spcn">
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>`;


var flotant_ibt2 =
`<div class="col-md-4 info_base_tipo2 flotant">
    <div class="flotant_header">
        <p class="ibt_title">Informacion base tipo 2</p>
        <div class="min_close_container">
            <span  data-cx="_" class="close">_</span>
            <span data-cx="X" class="close">X</span>
        </div>
    </div>
    <div class="flotant_body">
        <ul class="ibt2_list">
            <li class="ibt2_sinanpe_item"><label>Ecosistema/Ecorregión</label>
                <ul class="ibt2_sinanpe_list">
                    <li><label for="check_biomas">Biomas</label>
                        <input class="ibt_check" type="checkbox" name="check_biomas" id="check_biomas">
                        <ul class="bio_list">
                            <li>Bosques húmedos latifoliados tropicales y subtropicales</li>
                            <li>Bosques secos latifolia tropicales y subtropicales</li>
                            <li>Desiertos y amtorrales xéricos</li>
                            <li>Lagos</li>
                            <li>Manglares</li>
                            <li>Pastizales y matorrales tropicales y subtropicales</li>
                        </ul>
                    </li>
                    <li><label for="check_ecoterr">Ecorregiones Terrestres</label>
                        <input class="ibt_check" type="checkbox" name="check_ecoterr" id="check_ecoterr">
                        <ul class="ecoterr_list">
                            <li>Bosques húmedos de la Amazonia Sur Occidental</li>
                            <li>Bosques Humedos del napo</li>
                            <li>Bosques Humedos del Solimes</li>
                            <li>Bosques Humedor del Ucayali</li>
                            <li>Cordillera real Oriental</li>
                            <li>Lago titicaca</li>
                            <li>Desiertos de sechura</li>
                            <li>Paramos</li>
                            <li>Sabanas del Beni</li>
                        </ul>
                    </li>
                    <li><label for="check_ecomar">Ecorregiones Marinas</label>
                        <input class="ibt_check" type="checkbox" name="check_ecomar" id="check_ecomar">
                        <ul class="ecomar_list">
                            <li>Mar frio de la corriente peruana o de humbolt</li>
                            <li>Mar tropical</li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>`;

var add_map =
`<div class="adicionar_map_base col-md-3 flotant">
    <div class="flotant_header">
        <span class="add_map"><a href="#"><i class="fa fa-3x fa-plus-square icon-map" aria-hidden="true"></i></a></span>
        <span style="color: red">Añadir mapa base </span>
    </div>
    <div class="row flotant_body">
        <div class="col-xs-3 col-md-3"><a href="#"><img data-map="Relieve1" src="img/minimap.png" class="img-thumbnail" alt="">Relieve 1</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img data-map="Relieve2" src="img/minimap.png" class="img-thumbnail" alt="">Relieve 2</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img data-map="Relieve3" src="img/minimap.png" class="img-thumbnail" alt="">Relieve 3</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img data-map="Relieve4" src="img/minimap.png" class="img-thumbnail" alt="">Relieve 4</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img src="img/minimap.png" class="img-thumbnail" alt="">Relieve 5</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img src="img/minimap.png" class="img-thumbnail" alt="">Relieve 6</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img src="img/minimap.png" class="img-thumbnail" alt="">Relieve 7</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img src="img/minimap.png" class="img-thumbnail" alt="">Relieve 8</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img src="img/minimap.png" class="img-thumbnail" alt="">Relieve 9</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img src="img/minimap.png" class="img-thumbnail" alt="">Relieve 10</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img src="img/minimap.png" class="img-thumbnail" alt="">Relieve 11</a></div>
        <div class="col-xs-3 col-md-3"><a href="#"><img src="img/minimap.png" class="img-thumbnail" alt="">Relieve 12</a></div>
    </div>
    <div class="row flotant_report">
        <div class="report_header">
            <label style="margin-bottom: 0;">REPORTE:</label>
            <span class="label label-default" style="margin-left:10px">Representatividad Ecologica</span>
            <span class="close"  style="margin-left:auto">X</span>
        </div>
        <div class="report_body">
            <ul>
                <li style="margin-bottom:30px;"><i class="fa fa-2x fa-bars" aria-hidden="true"></i></li>
                <li><i class="fa fa-2x fa-angle-double-left" aria-hidden="true"></i></li>
                <li><i class="fa fa-2x fa-angle-double-left" aria-hidden="true"></i></li>
            </ul>
            <div class="col-md-4 report_info_left"></div>
            <div class="col-md-4 report_info_right"></div>
            <ul>
                <li style="margin-bottom:30px;"><i class="fa fa-2x fa-bars" aria-hidden="true"></i></li>
                <li><i class="fa fa-2x fa-angle-double-right" aria-hidden="true"></i></li>
                <li><i class="fa fa-2x fa-angle-double-right" aria-hidden="true"></i></li>
            </ul>  
        </div>
        <div class="report_footer">
            <button class="btn btn-default btn-md">OK</button>
            <button class="btn btn-default btn-md">Cancel</button>
        </div>
    </div>
</div>`;



