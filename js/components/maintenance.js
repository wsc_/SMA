var table_indicators = 
`<fieldset class="table_conf_ind">
    <legend>Configuración Indicador</legend>
    <table id="table_indicators" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Nivel 1</th>
                <th>Nivel 2</th>
                <th>nivel 3</th>
                <th>nivel 4</th>
                <th>Indicador</th>
                <th>Tipo Rep</th>
                <th>Tipo</th>
                <th>Capas</th>
                <th>Linea Base</th>
                <th>Parametros</th>
                <th>Restricción</th>
                <th>Activo</th>
            </tr>
        </thead>
    </table>
</fieldset>`; 

var select_indicator = 
`<div class="form-group">
    <select class="form-control" id="sel1">
        <option>Seleccione indicador</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
    </select>
</div>
<button class="btn btn-default">OK</button>
<button class="btn btn-default">Cancel</button>`;

var table_capas =
`<fieldset class="table_conf_ind">
    <legend>Lista de capas por categoria</legend>
    <table id="table_capas" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nivel 1</th>
                <th>Nivel 2</th>
                <th>Valor</th>
                <th>Unidad</th>
                <th>Reporte</th>
                <th>Listado</th>
            </tr>
        </thead>
    </table>
</fieldset>
`;

var table_linea_base =
`<fieldset class="table_conf_ind">
    <legend>Linea base de comparación</legend>
    <table id="table_lineabase" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Nivel 1</th>
                <th>Nivel 2</th>
                <th>Valor</th>
                <th>Unidad</th>
                <th>Reporte</th>
                <th>Listado</th>
            </tr>
        </thead>
    </table>
</fieldset>
`;

var table_parameters =
`<fieldset class="table_conf_ind params">
    <legend>Parametros</legend>
    <div class="col-md-12">
        <button class="btn btn-default"><i class="fa fa-plus-circle"></i></button>
        <input type="text" id="param_first">
        <select class="form-control" style="width: 20%;display: inline-block;">
            <option value="">Porcentaje</option>
            <option value="">Porcentaje</option>
            <option value="">Porcentaje</option>
        </select>
        <input class="form-control" type="number" placeholder="=" style="width: 10%;display: inline-block;" >
        <input class="form-control" type="number" placeholder="de 1 a 10 %" style="width: 20%;display: inline-block;" >
    </div>
    <div class="col-md-12">
        <button class="btn btn-default"><i class="fa fa-plus-circle"></i></button>
        <input type="text" id="param_second">
        <select class="form-control" style="width: 20%;display: inline-block;">
            <option value="">Porcentaje</option>
            <option value="">Porcentaje</option>
            <option value="">Porcentaje</option>
        </select>
        <input class="form-control" type="number" placeholder="=" style="width: 10%;display: inline-block;" >
        <input class="form-control" type="number" placeholder="mayor a 10 %" style="width: 20%;display: inline-block;" >
    </div>
    <div class="col-md-12">
        <button class="btn btn-default"><i class="fa fa-plus-circle"></i></button>
        <input type="text" id="param_third">
        <select class="form-control" style="width: 20%;display: inline-block;">
            <option value="">Porcentaje</option>
            <option value="">Porcentaje</option>
            <option value="">Porcentaje</option>
        </select>
        <input class="form-control" type="number" placeholder="=" style="width: 10%;display: inline-block;" >
        <input class="form-control" type="number" placeholder="menor a 1 %" style="width: 20%;display: inline-block;" >
    </div>
    <div class="col-md-12">
        <button class="btn btn-default"><i class="fa fa-plus-circle"></i></button>
        <input type="text" placeholder="numero" style="width: 30%;display: inline-block" class="form-control">
        <select class="form-control" style="width: 20%;display: inline-block;visibility: hidden;">
        </select>
        <input class="form-control" type="number" placeholder="=" style="width: 10%;display: inline-block;visibility: hidden;" >
        <input class="form-control" type="number" placeholder="menor a 1 %" style="width: 20%;display: inline-block;visibility: hidden;" >
    </div>
</fieldset>`;

var table_restricts =
`<fieldset class="table_conf_ind restricts">
    <legend>Restricciones</legend> 
    <div class="col-xs-12 col-md-12">   
        <div class="form-group">
            <label for="capa" class="control-label">Capa</label>
            <select name="" id="capa" class="form-control" style="width: 25%">
                <option value="">ANP</option>
                <option value="">SINANPE</option>
            </select>
        </div>
    </div>
    <div class="col-md-12" style="display: flex;justify-content: space-between;">
        <div class="form-group" style="width: 25%;display: inline-block">
            <label for="campo" class="control-label">Campo</label>
            <select name="" id="campo" class="form-control" >
                <option value="">ANP_nom</option>
                <option value="">SINANPE_nom</option>
            </select>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="">
                    Verificar
                </label>
            </div>
        </div>
        <div class="form-group" style="width: 25%;display: inline-block">
            <label for="condicion" class="control-label">Condición</label>
            <select name="condicion" id="condicion" class="form-control">
                <option value="">=</option>
                <option value="">option</option>
            </select>
        </div>
        <div class="form-group" style="width: 25%;display: inline-block">
            <label for="valor" class="control-label">valor</label>
            <select multiple class="form-control">
                <option>Pacaya Samira</option>
                <option>Pantanos de Villa</option>
                <option>Nueva ANP</option>
            </select>
        </div>    
    </div>  
    <div class="col-md-12">
    <input type="text" value="Not (Sector = XXX)" id="" class="form-control">
    </div>  
</fieldset>`;