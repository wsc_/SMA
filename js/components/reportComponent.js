var report_component =
`<div class="report_container_main">
    <div class="col-md-12 report_container">
        <div class="row df-c" style="margin: 0">
            <ul class="controls_report">
                <li><a class="report_option_map" href="#"><i class="fa fa-3x fa-map" aria-hidden="true"></i></a></li>
                <li><a class="report_option_globe" href="#"><i class="fa fa-3x fa-globe" aria-hidden="true"></i></a></li>
                <li><a class="report_option_chart" href="#"><i class="fa fa-3x fa-bar-chart" aria-hidden="true"></i></a></li>
                <li><a class="report_option_calendar" href="#"><i class="fa fa-3x fa-calendar" aria-hidden="true"></i></a></li>
                <li><a class="report_option_undo" href="#"><i class="fa fa-3x fa-undo" aria-hidden="true"></i></a></li>
                <li><a class="report_option_thlarge" href="#"><i class="fa fa-3x fa-th-large" aria-hidden="true"></i></a></li>
                <li><a class="report_option_ol" href="#"><i class="fa fa-3x fa-list-ol" aria-hidden="true"></i></a></li>     
            </ul>
        </div>
        <div class="row df-c" style="margin: 0;border-top: 2px solid black;border-bottom: 2px solid black;padding-bottom: 5px;padding-top: 5px;">
            <ul class="nav nav-pills">
                <li class="active"><a href="#sinanpe_tab" role="tab" data-toggle="tab">SINANPE</a></li>
                <li><a href="#anp_tab" role="tab" data-toggle="tab">ANP</a></li>
            </ul>
        </div>    
        <div class="row df-c" style="margin: 0 ;padding-top: 5px; padding-bottom: 5px;">
            <ul class="nav nav-pills">
                <li class="active"><a href="#comp_fisc" role="tab" data-toggle="tab">Componete Fisico</a></li>
                <li><a href="#est_cons" role="tab" data-toggle="tab">Estado de Conservación</a></li>
            </ul>
        </div>
        <div class="row" style="margin: 0 ;padding-top: 5px; padding-bottom: 5px;border-bottom: 2px solid black;">
            <div class="col-md-12">
                <div class="form-group" style="display:block">
                    <select name="periodo" id="periodo" class="form-control">
                        <option value="periodo">Periodo</option>
                        <option value="lastperiodo">Ultimo Periodo</option>
                    </select>
                </div>
                <div class="form-group" style="width: 45%;display: inline-block">
                    <label for="">Desde: </label>
                    <input class="form-control date_periodo_desde" type="date" placeholder="=" style="width: 100%;display: inline-block;" >
                </div>
                <div class="form-group" style="width: 45%;display: inline-block">
                    <label for="">Hasta: </label>
                    <input class="form-control date_periodo_hasta" type="date" placeholder="=" style="width: 100%;display: inline-block;" >
                </div>
                <button class="btn btn-default btn-aceptar_periodo">Aceptar</button>
            </div>
        </div>
        <div class="row" style="margin: 0 ;padding-top: 5px; padding-bottom: 5px;max-height: 450px;overflow: auto">
            <div class="panel-group panel-main" id="accordion" role="tablist" aria-multiselectable="true" style="display: none">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Composición
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="panel-group" id="accordion_child" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rep_ec">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion_child" href="#collapserRep_ec" aria-expanded="true" aria-controls="collapseOne">
                                            Representatividad Ecológica
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapserRep_ec" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rep_ec">
                                        <div class="panel-body">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="def_infb">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion_child_c" href="#collapseDef_infb" aria-expanded="true" aria-controls="collapseOne">
                                                        Definir Información base:
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseDef_infb" class="panel-collapse collapse" role="tabpanel" aria-labelledby="def_infb">
                                                    <div class="panel-body">
                                                        <ul class="ibt1_list">
                                                            <h4>Informacion Base Tipo 1</h4>
                                                            <li class="ibt1_sinanpe_item"><label for="check_sinanpe">SINANPE</label>
                                                                <input class="ibt_check" type="checkbox" name="check_sinanpe" id="check_sinanpe">
                                                                <ul class="ibt1_sinanpe_list">
                                                                    <li><label for="check_anp">Áreas naturales protegidas definitivas(ANP)</label>
                                                                        <input class="ibt_check" type="checkbox" name="check_anp" id="check_anp">
                                                                        <ul class="anp_list">
                                                                            <li>Parques Nacionales</li>
                                                                            <li>Santuario Nacionales</li>
                                                                            <li>Santuarios Históricos</li>
                                                                            <li>Reservas Nacionales</li>
                                                                            <li>Refugios de Vida Silvestre</li>
                                                                            <li>Reservas Paisajísticas</li>
                                                                            <li>Reservas Comunales</li>
                                                                            <li>Bosques de Protección</li>
                                                                            <li>Cotos de Caza</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li><label for="check_anpt">Áreas Naturales Protegidas transitorias</label>
                                                                        <input class="ibt_check" type="checkbox" name="check_anpt" id="check_anpt">
                                                                        <ul class="anpt_list">
                                                                            <li>Zonas Reservadas(ZR)</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li><label for="check_acr">Áreas de Conservación Regional(ACR)</label>
                                                                        <input class="ibt_check" type="checkbox" name="check_acr" id="check_acr">
                                                                    </li>
                                                                    <li><label for="check_acp">Áreas de Conservación Privada(ACP)</label>
                                                                        <input class="ibt_check" type="checkbox" name="check_acp" id="check_acp">
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li class="ibt1_sinanpe_item"><label for="check_oaa">Otras áreas para el analisis</label>
                                                                <input class="ibt_check" type="checkbox" name="check_oaa" id="check_oaa">
                                                                <ul class="oaa_ist">
                                                                    <li><a href="#">Concesiones de conservación</a>
                                                                        <input class="ibt_check" type="checkbox" name="check_cdc">
                                                                    </li>
                                                                    <li><a href="#">Concesiones de ecoturismo</a>
                                                                        <input class="ibt_check" type="checkbox" name="check_cde" >
                                                                    </li>
                                                                    <li><a href="#">Concesiones de aprovechamiento forestal</a>
                                                                        <input class="ibt_check" type="checkbox" name="">
                                                                    </li>
                                                                    <li><a href="#">Reservas Territoriales</a>
                                                                        <input class="ibt_check" type="checkbox" name="check_rt">
                                                                    </li>
                                                                    <li><a href="#">Reservas indígenas</a>
                                                                        <input class="ibt_check" type="checkbox" name="check_ris">
                                                                    </li>
                                                                    <li><a href="#">Zonas de amortiguamiento(ZA)</a>
                                                                        <input class="ibt_check" type="checkbox" name="check_za">
                                                                    </li>
                                                                    <li><a href="#">Reservas de Biosfera</a>
                                                                        <input class="ibt_check" type="checkbox" name="check_rb">
                                                                    </li>
                                                                    <li><a href="#">Sitios de Patrimonio Mundial Natural de la UNESCO</a>
                                                                        <input class="ibt_check" type="checkbox" name="check_spmnu" >
                                                                    </li>
                                                                    <li><a href="#">Sitios de Patrimonio(Cultural y Natural) de la UNESCO</a>
                                                                        <input class="ibt_check" type="checkbox" name="check_spcn">
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                        <p><a class="next_ibt1" href="#" style="float: right;">Siguiente <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                                                        <ul class="ibt2_list" style="display: none">
                                                            <h4>Informacion Base Tipo 2</h4>
                                                            <li class="ibt2_sinanpe_item"><label>Ecosistema/Ecorregión</label>
                                                                <ul class="ibt2_sinanpe_list">
                                                                    <li><label for="check_biomas">Biomas</label>
                                                                        <input class="ibt_check" type="checkbox" name="check_biomas" id="check_biomas">
                                                                        <ul class="bio_list">
                                                                            <li>Bosques húmedos latifoliados tropicales y subtropicales</li>
                                                                            <li>Bosques secos latifolia tropicales y subtropicales</li>
                                                                            <li>Desiertos y amtorrales xéricos</li>
                                                                            <li>Lagos</li>
                                                                            <li>Manglares</li>
                                                                            <li>Pastizales y matorrales tropicales y subtropicales</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li><label for="check_ecoterr">Ecorregiones Terrestres</label>
                                                                        <input class="ibt_check" type="checkbox" name="check_ecoterr" id="check_ecoterr">
                                                                        <ul class="ecoterr_list">
                                                                            <li>Bosques húmedos de la Amazonia Sur Occidental</li>
                                                                            <li>Bosques Humedos del napo</li>
                                                                            <li>Bosques Humedos del Solimes</li>
                                                                            <li>Bosques Humedor del Ucayali</li>
                                                                            <li>Cordillera real Oriental</li>
                                                                            <li>Lago titicaca</li>
                                                                            <li>Desiertos de sechura</li>
                                                                            <li>Paramos</li>
                                                                            <li>Sabanas del Beni</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li><label for="check_ecomar">Ecorregiones Marinas</label>
                                                                        <input class="ibt_check" type="checkbox" name="check_ecomar" id="check_ecomar">
                                                                        <ul class="ecomar_list">
                                                                            <li>Mar frio de la corriente peruana o de humbolt</li>
                                                                            <li>Mar tropical</li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                        <p><a class="back_ibt2" href="#" style="float: left;display: none"><i class="fa fa-angle-double-left" aria-hidden="true"></i>Atras</a></p>
                                                        <p><a class="next_ibt2" href="#" style="float: right;display: none">Siguiente <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                                                        <div class="cont_result_selects" style="display: none">
                                                            <div class="form-group forg_sel_tb1">
                                                                <label for="sel_bt1" class="control-label">Selección Base tipo 1</label>
                                                                <select multiple class="form-control sel_bt1" style="display: inline-block;width: 85%">
                                                                </select>
                                                                <div class="action_to_select" style="display: inline-block;">
                                                                    <a href="#" style="display: block"><i class="fa fa-2x fa-check-circle" aria-hidden="true" style="color: green"></i></a>
                                                                    <a href=""><i class="fa fa-2x fa-times-circle" aria-hidden="true" style="color: green"></i></a>
                                                                </div>
                                                            </div>
                                                            <div class="form-group forg_sel_tb2">
                                                                <label for="sel_bt2" class="control-label">Selección Base tipo 2</label>
                                                                <select multiple class="form-control sel_bt2" style="display: inline-block;width: 85%">
                                                                </select>
                                                                <div class="action_to_select" style="display: inline-block;">
                                                                    <a href="#" style="display: block"><i class="fa fa-2x fa-check-circle" aria-hidden="true" style="color: steelblue"></i></a>
                                                                    <a href=""><i class="fa fa-2x fa-times-circle" aria-hidden="true" style="color: steelblue"></i></a>
                                                                </div>
                                                            </div>
                                                            <p><a class="acept_selects_bt" href="#" style="float: right;">Aceptar</a></p>
                                                        </div>             
                                                        <div class="panel panel-default panel_result_selectes" style="display: none">
                                                            <div class="panel-heading" role="tab" id="por_c_">
                                                                <h4 class="panel-title">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion_child_x" href="#collapsePor_c_" aria-expanded="true" aria-controls="collapseOne">
                                                                    Porcentaje de cobertura ecológica dentro de SINANPE(%)
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapsePor_c_" class="panel-collapse collapse" role="tabpanel" aria-labelledby="por_c_">
                                                                <div class="panel-body">
                                                                    <ul class="nav nav-pills nav-stacked result_final_select">
                                                                        <li data-option="sbrs15/12/2017"><a href="#">Superficie de Bioma representados en el SINANPE 15/12/2017</a></li>
                                                                        <li data-option="setr11/11/2017"><a href="#">Superficie de Ecorregión terrestre representada en el SINANPE 11/11/2017</a></li>
                                                                        <li data-option="semr"><a href="#">Superficie de Ecorregión marina representada en el SINANPE</a></li>
                                                                        <li data-option="prb01/04/2018"><a href="#">Porcentaje de Representatividad de los Biomas 01/04/2018</a></li>
                                                                        <li data-option="pret20/01/2018"><a href="#">Porcentaje de Representatividad de Ecorregiones terrestres 20/01/2018</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="rec_terr">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion_child" href="#collapseRec_terr" aria-expanded="true" aria-controls="collapseOne">
                                            Representatividad Territorial
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseRec_terr" class="panel-collapse collapse" role="tabpanel" aria-labelledby="rec_terr">
                                        <div class="panel-body">
                                            <a href="#">Definir Información base:</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="Consist">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion_child" href="#collapseConsist" aria-expanded="true" aria-controls="collapseOne">
                                            Consistencia
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseConsist" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Consist">
                                        <div class="panel-body">
                                            <a href="#">Definir Información base:</a>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Conectividad
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Forma
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9 container_charts" style="display: none;">
        <div id="chartContainer" style="height: 370px; width: 60%;"></div>
        <div class="form-group" style="    display: inline-block;margin-left: 20px">
            <select name="estadi_per" id="estadi_per" class="form-control">
                <option value="">Estadistica anual</option>
                <option value="">Estadistica mensual</option>
            </select>
            <div class="checkbox">
                <label>
                    <input class="chart_check_linebase" type="checkbox" value="">
                    Linea base
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-9 container_map_report" style="display: none">
        <div class="col-md-12 df-c">
            <img src="img/peru.jpg" style="width: 40%;height: 100%;" >
        </div>
    <div class="col-md-12">
            <input type="range" name="points" id="points" value="50" min="0" max="100" data-show-value="true">
    </div>
    </div>
    <div class="col-md-9 container_4_blocks" style="display: none;">
        <div class="col-md-6">
            <div class="col-md-12 df-c">
                <img src="img/peru.jpg" style="width: 40%;height: 100%;" >
            </div>
            <div class="col-md-12">
                <input type="range" name="points" id="points" value="50" min="0" max="100" data-show-value="true">
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12 df-c">
                <img src="img/peru.jpg" style="width: 40%;height: 100%;" >
            </div>
            <div class="col-md-12">
                <input type="range" name="points" id="points" value="50" min="0" max="100" data-show-value="true">
            </div>
        </div>
        <div class="col-md-6">
            <div id="chartContainer2" style="height: 370px; width: 80%;"></div>
            <div class="form-group" style="    display: inline-block;margin-left: 20px">
                <select name="estadi_per" id="estadi_per" class="form-control">
                    <option value="">Estadistica anual</option>
                    <option value="">Estadistica mensual</option>
                </select>
                <div class="checkbox">
                    <label>
                        <input class="chart_check_linebase" type="checkbox" value="">
                        Linea base
                    </label>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <table class="table table-dark">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">First</th>
                    <th scope="col">Last</th>
                    <th scope="col">Handle</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                    </tr>
                    <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                    </tr>
                    <tr>
                    <th scope="row">3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>@twitter</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>`;