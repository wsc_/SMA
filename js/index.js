
/********************SINANPE REPRESENTATIVIDA ECOLOGICA******************/

function ecologicalRepresentativity(con, flotantibt1, flotantibt2, addmap){
    //append flotants and controls
    var item_menu   = $('.menu-nav li a'),
        content     = $('.container-fluid'),
        isActive    = false;
    item_menu.on('click', function(ev){ 
        console.log(ev)
        if(isActive == false){
            content.empty();
            var item_menu = $(this).attr('data-nav');
            if(item_menu === 'Representatividad Ecológica'){
                content.append(con);
                content.append(flotantibt1);
                content.append(flotantibt2);
                content.append(addmap);
                isActive = !isActive;
            } 
        }else{
            content.empty();
            isActive = !isActive;
        }
    }); 
    //append data to flotand
    var mini_map = '.adicionar_map_base .flotant_header a i',
        isOpen  = false;
    $(".container-fluid").on('click', mini_map, function(ev){
        if(isOpen == false){
            $(".adicionar_map_base .flotant_body").css("display", "block");
            isOpen = !isOpen;
        }else{
            $(".adicionar_map_base .flotant_body ").css("display", "none");
            isOpen = !isOpen;
        }

    });
    //add minimaps base

    //close and minime flotans
    var isMinize = false;
    $(".container-fluid").on('click', '.info_base_tipo1 .close', function(ev){
        var action = $(this).attr('data-cx');
        if(action === 'X'){
            $('.info_base_tipo1').remove();
        }else if(action === '_'){
            if(isMinize == false){
                $(".info_base_tipo1 .flotant_body ").css("display", "none");
                isMinize = !isMinize;
            }else{
                $(".info_base_tipo1 .flotant_body ").css("display", "block");
                isMinize = !isMinize;
            }    
        }
    });
    var isMinize2 = false;
    $(".container-fluid").on('click', '.info_base_tipo2 .close', function(ev){
        var action = $(this).attr('data-cx');
        if(action === 'X'){
            $('.info_base_tipo2').remove();
        }else if(action === '_'){
            if(isMinize2 == false){
                $(".info_base_tipo2 .flotant_body ").css("display", "none");
                isMinize2 = !isMinize2;
            }else{
                $(".info_base_tipo2 .flotant_body ").css("display", "block");
                isMinize2 = !isMinize2;
            }    
        }
    });
    $(".container-fluid").on("click", ".adicionar_map_base .close", function(ev){
        $('.adicionar_map_base').remove();
    });
    //click on checkbox
    $(".container-fluid").on("click", ".info_base_tipo1 .ibt_check", function(ev){
        var isChecked = $(this).is(':checked');
        if(isChecked === true){
            var option_checked = $(this).prev().text();
            $(".report_info_left").empty();
            $(".report_info_left").append("<p>"+option_checked+"</p>");
        }
    });
    $(".container-fluid").on("click", ".info_base_tipo2 .ibt_check", function(ev){
        var isChecked = $(this).is(':checked');
        if(isChecked === true){
            var option_checked = $(this).prev().text();
            $(".report_info_right").empty();
            $(".report_info_right").append("<p>"+option_checked+"</p>");
        }
    });
}


/********************************MANTENIMIENTO***************************/
function maintenanceTables(tableindicators, selectindicator, tablecapas, tablelineabase, tableparameters, tablerestricts){
    var item_mant = $(".mantenim_item"),
        parent = $(".container-fluid");
    item_mant.on('click', function(){
        var table_main = $(".table_conf_ind").length;
        if(table_main < 0){
            parent.empty();
        }else{
            parent.append(tableindicators);
            maintenanceDataTable(selectindicator, tablecapas, tablelineabase, tableparameters, tablerestricts);
        }
    });
}
function maintenanceDataTable(selectindicator, tablecapas, tablelineabase, tableparameters, tablerestricts){
    var table = $('#table_indicators').DataTable({
        ajax: "js/data/arrays.txt",
        dom: '<"toolbar"><"buttons">frtip',
        searching: false,
        responsive: true,
        "columnDefs": [{
            "targets": -1,
            "data": null,
            "defaultContent": '<input type="checkbox" class="checkbox activo">'
        },{
            "targets": -2,
            "data": null,
            "defaultContent": '<button data-type="restric" class="btn btn-default">Seleccionar</button>'
        },
        {
            "targets": -3,
            "data": null,
            "defaultContent": '<button data-type="parametros" class="btn btn-default">Seleccionar</button>'
        },
        {
            "targets": -4,
            "data": null,
            "defaultContent": '<button data-type="lineabase" class="btn btn-default">Seleccionar</button>'
        },
        {
            "targets": -5,
            "data": null,
            "defaultContent": '<button data-type="capas" class="btn btn-default">Seleccionar</button>'
        }]

    });
    $("div.toolbar").html(selectindicator);
    var mod = $('#modalMant'),
        mod_body = $('.modal-body');
    $('#table_indicators tbody').on( 'click', 'button', function () {
        var data_type = $(this).attr('data-type');
        switch (data_type){
            case 'capas':
                mod_body.append(tablecapas);
                tableCapas();
                mod.modal('show');
                break;
            case 'lineabase' :
                mod_body.append(tablelineabase);   
                tableLineaBase();
                mod.modal('show');
                break;
            case 'parametros' :
                mod_body.append(tableparameters);
                spectrum_init();   
                mod.modal('show');
                break;
            case 'restric':
                mod_body.append(tablerestricts);
                mod.modal('show');
        }
    });
}

function tableCapas(){
    var table = $('#table_capas').DataTable({
        ajax: "js/data/data.json",
        "lengthMenu": [ 4, 8],
        responsive: true,
        "columnDefs": [{
            "targets": -1,
            "data": null,
            "defaultContent": '<input type="checkbox" class="checkbox listado">'
        },{
            "targets": -2,
            "data": null,
            "defaultContent": '<input type="checkbox" class="checkbox reporte">'
        }]

    });
    /*new $.fn.dataTable.Buttons( table, {
        buttons: [
            'copy', 'excel', 'pdf'
        ]
    } );*/
}
function tableLineaBase(){
    var table = $('#table_lineabase').DataTable({
        ajax: "js/data/data2.json",
        "lengthMenu": [ 4, 8],
        responsive: true,
        "columnDefs": [{
            "targets": -1,
            "data": null,
            "defaultContent": '<input type="checkbox" class="checkbox listado">'
        },{
            "targets": -2,
            "data": null,
            "defaultContent": '<input type="checkbox" class="checkbox reporte">'
        }]

    });
}
/***********************REPORTE ************************************/
function report(reportcomponent){
    //click report menu print data
    $(".report_item").on("click", function(){
        var parent = $(".container-fluid");
        parent.empty();
        parent.append(reportcomponent);
    });
    //click on checkbox info base tipo 1
    $(".container-fluid").on("click", ".panel-body .ibt1_list .ibt_check", function(ev){
        console.log("click")
        var isChecked = $(this).is(':checked');
        if(isChecked === true){
            var option_checked = $(this).prev().text();
            $(".forg_sel_tb1 .sel_bt1").append("<option value="+option_checked+">"+option_checked+"</option>");
        }
    });
    //next info base tipo 1
    $(".container-fluid").on("click", ".next_ibt1", function(){
        var selected = [];
        $('.ibt1_list input:checked').each(function() {
            selected.push($(this).attr("name"));
        });
        if(selected.length == 0){
            alert("Seleccione Informacion base tipo 1");
        }else{
            $(this).css("display", "none");
            $(".panel-body .ibt1_list").css("display", "none");
            $(".panel-body .ibt2_list").css("display", "block");
            $(".next_ibt2").css("display", "block");
            $(".back_ibt2").css("display", "block");
        }
    })
    //back to info base 1 
    $(".container-fluid").on("click",".back_ibt2", function(){
        $(".panel-body .ibt2_list").css("display", "none");
        $(".next_ibt2").css("display", "none");
        $(this).css("display", "none");
        $(".panel-body .ibt1_list").css("display", "block");
        $(".next_ibt1").css("display", "block");
    });
    //click on checkbox info base tipo 2
    $(".container-fluid").on("click", ".panel-body .ibt2_list .ibt_check", function(ev){
        var isChecked = $(this).is(':checked');
        if(isChecked === true){
            var option_checked = $(this).prev().text();
            $(".forg_sel_tb2 .sel_bt2").append("<option value="+option_checked+">"+option_checked+"</option>");
        }
    });
     //next info base tipo 2
     $(".container-fluid").on("click",".next_ibt2", function(){
        var selected = [];
        $('.ibt2_list input:checked').each(function() {
            selected.push($(this).attr("name"));
        });
        if(selected.length == 0){
            alert("Seleccione Informacion base tipo 2");
        }else{
            $(this).css("display", "none");
            $(".back_ibt2").css("display", "none");
            $(".panel-body .ibt2_list").css("display", "none");
            $(".cont_result_selects").css("display", "block");
        }
    })
    //select periodo
    $("#periodo").on("change", function(){
        var val_periodo =$(this).val();
        if(val_periodo == "periodo"){
            $(".date_periodo_desde").removeAttr("disabled");
            $(".date_periodo_hasta").removeAttr("disabled");
        }else{
            $(".date_periodo_desde").attr("disabled", "disabled");
            $(".date_periodo_hasta").attr("disabled", "disabled");
        }
    });
    //click aceptar periodo
    $(".container-fluid").on("click",".btn-aceptar_periodo", function(){
        var options = $("#periodo").val();
        if(options == "periodo"){
            var desde = $(".date_periodo_desde").val();
            var hasta = $(".date_periodo_hasta").val();
            console.log(desde);
            console.log(hasta);
            if(desde == "" || hasta == ""){
                alert("Seleccione fechas para el periodo");
            }else{
                $(".panel-main").css("display", "block");
            }
        }else if(options == "lastperiodo"){
            $(".panel-main").css("display", "block");
        }else {
            alert("Seleccione  el periodo");
        }
    });
    //click aceptar selecciones de base tipo
    $(".container-fluid").on("click",".acept_selects_bt", function(){
        $(".cont_result_selects").css("display", "none");
        $(".panel_result_selectes").css("display", "block");
    });
    //add active to list final
    $(".container-fluid").on("click", ".result_final_select li", function(){
        $(".result_final_select").find(".active").removeClass("active");
        $(this).addClass("active");
    });
    // click report_option_chart
    $(".container-fluid").on("click", ".report_option_chart", function(){
        var hasActive = $(".result_final_select li").hasClass("active");
        if(hasActive == true){
            var result_select = $(".result_final_select").find(".active")[0].attributes["0"].value;
            $(".container_map_report").css("display", "none");
            $(".container_4_blocks").css("display", "none");
            $(".container_charts").css("display", "flex");
            chartBarReport();
        }else{
            alert("selecccione una opcion de la lista")
        }
    });
    // click report_option_map
    $(".container-fluid").on("click",".report_option_undo", function(){
        var hasActive = $(".result_final_select li").hasClass("active");
        if(hasActive == true){
            var result_select = $(".result_final_select").val()[0];
            $(".container_charts").css("display", "none");
            $(".container_4_blocks").css("display", "none");
            $(".container_map_report").css("display", "block");
            
        }else{
            alert("selecccione una opcion de la lista")
        }
       
    });
       // click for blocks
       $(".container-fluid").on("click",".report_option_thlarge", function(){
        var hasActive = $(".result_final_select li").hasClass("active");
        if(hasActive == true){
            var result_select = $(".result_final_select").val()[0];
            $(".container_charts").css("display", "none");
            $(".container_map_report").css("display", "none");
            $(".container_4_blocks").css("display", "block");
            chartBarReport2();
        }else{
            alert("selecccione una opcion de la lista")
        }
    });
    
}
/*************************INIT COMPONENTS ***************/
function modal_init(){
    var mod_body = $('.modal-body');
    $('#modalMant').modal({
        keyboard: false,
        backdrop: 'static',
        show : false
    });
    $('#modalMant').on('hidden.bs.modal', function (e) {
        mod_body.empty();
    })
}
function spectrum_init(){
    var options = {
        showInput: true,
        allowEmpty:true,
        color: 'yellow',
        chooseText: "Elegir",
        cancelText: "Cancelar",
        preferredFormat: "hex"
    }
    $("#param_first").spectrum(options);
    options.color = 'green';
    $("#param_second").spectrum(options);
    options.color = 'red';
    $("#param_third").spectrum(options);
}

/*************************INIT FUNCTIONS AND DATA ***************/
//charge dataecologicalRepresentativity
var con                 = controls,
    flotantibt1         = flotant_ibt1,
    flotantibt2         = flotant_ibt2,
    addmap              = add_map;

//charge maintenanceTables
var tableindicators     = table_indicators,
    selectindicator     = select_indicator,
    tablecapas          = table_capas,
    tablelineabase      = table_linea_base,
    tableparameters     = table_parameters,
    tablerestricts     = table_restricts,
    reportcomponent     = report_component;

//methods
ecologicalRepresentativity(con, flotantibt1, flotantibt2, addmap, tablecapas);
maintenanceTables(tableindicators, selectindicator, tablecapas, tablelineabase, tableparameters, tablerestricts);
report(reportcomponent);
modal_init();
spectrum_init();